/**
*@Title HbaseTest.java
*@description TODO
*@time 2020年5月27日 下午2:07:21
*@author liuyijiao
*@version 1.0
**/
package utils;

import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;

/**
 * @author liuyijiao
 *
 */
public class HbaseTest {

	public static void main(String[] args) {
		//HbaseUtil.createTable("lyj888", new String[] {"info"});
		//HbaseUtil.putRow("lyj888", "zhangsan", "info", "age", "11");
		//HbaseUtil.putRow("lyj888", "zhangsan", "info", "sex", "男");
		ResultScanner scanner=HbaseUtil.getScanner("lyj888");
		HbaseTest.listValue(scanner);
		//HbaseConn.closeConn();
		
	}
	 public static void listValue(ResultScanner scanner) {
	        for (Result next : scanner) {
	            byte[] value = next.getValue("info".getBytes(), "age".getBytes());
	            byte[] name = next.getValue("info".getBytes(), "sex".getBytes());
	            System.out.println(new String(value));
	            System.out.println(new String(name));
	            System.out.println(new String(next.getRow()));
	            System.out.println("***************");

	        }
	 }
}
